<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'en' => [
                'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
                'content' => $this->faker->paragraph(
                    $nbSentences = 20, $variableNbSentences = true
                ),
            ],
            'ru' => [
                'title' => 'ПЕРЕВОД НА РУССКИЙ - ' . $this->faker->sentence($nbWords = 6, $variableNbWords = true),
                'content' => 'ПЕРЕВОД НА РУССКИЙ - ' . $this->faker->paragraph(
                        $nbSentences = 20, $variableNbSentences = true
                    ),
            ]

        ];
    }
}
