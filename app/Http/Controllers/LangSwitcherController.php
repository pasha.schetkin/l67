<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LangSwitcherController extends Controller
{
   public function switcher(Request $request, string $locale)
   {
       $request->session()->put('locale', $locale);
       return back();
   }
}
