<?php

namespace App\View\Components;

use App\Models\Comment;
use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class CommentComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @param Comment $comment
     */
    public function __construct(
        public Comment $comment
    )
    {}

    public function render(): View|Factory|Htmlable|Closure|string|Application
    {
        return view('components.comment-component');
    }
}
