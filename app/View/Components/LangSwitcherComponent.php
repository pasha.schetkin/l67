<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class LangSwitcherComponent extends Component
{
    public string $locale;

    public array $locales = [
        'ru',
        'en'
    ];
    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        $this->locale = session()->get('locale', config('app.locale', 'en'));
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.lang-switcher-component');
    }

    public function selected(string $locale): string
    {
        if ($locale === $this->locale) return 'selected';
        return '';
    }
}
