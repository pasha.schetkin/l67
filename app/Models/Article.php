<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

/**
 * Class Article
 * @package App\Models
 *
 * @property string title
 * @property string content
 * @property string user_id
 *
 * @property User user
 * @property Collection|Comment[] comments
 */
class Article extends AbstractModel implements TranslatableContract
{
    use Translatable;

    protected $fillable = ['user_id'];

    public $translatedAttributes = [
        'title', 'content'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class)->orderBy('id', 'desc');
    }
}
