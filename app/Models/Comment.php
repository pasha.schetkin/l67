<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Comment
 * @package App\Models
 *
 * @property int article_id
 * @property int user_id
 * @property string body
 *
 * @property User user
 * @property Article article
 */
class Comment extends AbstractModel
{
    protected $fillable = ['article_id', 'user_id', 'body'];

    /**
     * @return BelongsTo
     */
    public function article(): BelongsTo
    {
        return $this->belongsTo(Article::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
