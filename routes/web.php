<?php

use App\Http\Controllers\ArticlesCommentsController;
use App\Http\Controllers\ArticlesController;
use App\Http\Controllers\LangSwitcherController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('lang-switcher')->group(function () {
    Route::get('/', [ArticlesController::class, 'index'])->name('dashboard');
    Route::resource('articles', ArticlesController::class);
    Route::delete('articles/{article}/destroy', [ArticlesController::class, 'delete'])->name('articles.delete');

    Route::resource('articles.comments', ArticlesCommentsController::class)->only(['store', 'destroy']);

    Auth::routes();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('lang/{locale}', [LangSwitcherController::class, 'switcher'])
        ->name('lang.switcher')
        ->where('locale', 'en|ru');
});
